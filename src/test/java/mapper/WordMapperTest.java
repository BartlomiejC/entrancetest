package mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WordMapperTest {
    WordMapper wordMapper;

    @BeforeEach
    public void initialize(){
        wordMapper = new WordMapper();
    }

    @Test
    public void shouldThrowExceptionWhenGivenNullInput(){
        assertThrows(NullPointerException.class,
                ()-> wordMapper.mapLettersWithWordsContainingThem(null));
    }

    @Test
    public void shouldIncludeAllLetters(){
        String inputSentence = "Buying a 2*ship...";
        List<Character> distinctLettersOfInputSentence = Arrays.asList('b','u','y','i','n','g','a','s','h','p');
        Map<Character, Set<String>> result = wordMapper.mapLettersWithWordsContainingThem(inputSentence);

        assertTrue(result.keySet().containsAll(distinctLettersOfInputSentence));
    }

    @Test
    public void shouldIgnoreRepetitiveWords(){
        String inputSentence = "To be, or not to be.";
        Map<Character, Set<String>> result = wordMapper.mapLettersWithWordsContainingThem(inputSentence);

        assertTrue(result.get('t').size()==2);
        assertTrue(result.get('t').contains("to"));
        assertTrue(result.get('t').contains("not"));

        assertTrue(result.get('b').size()==1);
        assertTrue(result.get('b').contains("be"));
    }
}