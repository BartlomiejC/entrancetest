package mapper;

import java.util.*;
import java.util.stream.Collectors;

public class WordMapper {
    private final String NON_LETTERS_REGEX = "[^a-zA-Z]+";

    public Map<Character, Set<String>> mapLettersWithWordsContainingThem(String inputSentence) {
        Objects.requireNonNull(inputSentence);

        Set<String> words = findDistinctWords(inputSentence);
        return addMappingsForWords(words);
    }

    private Set<String> findDistinctWords(String inputSentence) {
        return Arrays.stream(inputSentence.split(NON_LETTERS_REGEX))
                .map(word -> word.toLowerCase())
                .collect(Collectors.toSet());
    }

    private Map<Character, Set<String>> addMappingsForWords(Set<String> words) {
        Map<Character, Set<String>> resultingMappings = new HashMap<>();

        for (String word : words) {
            for (Character character : word.toCharArray()) {
                addMapping(resultingMappings, word, character);
            }
        }

        return resultingMappings;
    }

    private void addMapping(Map<Character, Set<String>> mappings, String word, Character character) {
        if (mappings.containsKey(character)) {
            mappings.get(character).add(word);
        } else {
            Set<String> listForNewCharacter = new TreeSet<>();
            listForNewCharacter.add(word);
            mappings.put(character, listForNewCharacter);
        }
    }
}
