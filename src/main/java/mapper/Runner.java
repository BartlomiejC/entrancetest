package mapper;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Runner {
    public static void main(String[] args) {
        System.out.println("Provide sentence:");
        Scanner scan = new Scanner(System.in);

        WordMapper wordMapper = new WordMapper();
        Map<Character, Set<String>> result = wordMapper.mapLettersWithWordsContainingThem(scan.nextLine());

        System.out.println("Mappings:");
        System.out.println(result);
    }
}
